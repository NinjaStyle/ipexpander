# README #
##Expand CIDR address ranges to individual IP addresses.##
Usage: python ipexpander.py <filename>

Example:
```
# printf "10.0.0.0/24\n192.168.0.0/24" > test.txt
# python ipexpander.py test.txt 
10.0.0.1
10.0.0.2
...
192.168.0.254
192.168.0.255
```