##Expand CIDR address ranges to individual IP addresses.
##Requires iptools library (pip install iptools)
import iptools
import sys
iplist = []
if len(sys.argv) != 2:
    print "Usage: python ipexpander.py <filename>"
    print "Usage: python ipexpander.py <cidr range>"
    exit()

fname = sys.argv[1]
if iptools.ipv4.validate_cidr(fname):
    l = fname
    r = iptools.IpRangeList(l)
else:
    with open(fname) as f:
        l = f.readlines()
    #strip newlines   
    l = [i.strip() for i in l]
    #create rangelist
    r = iptools.IpRangeList(*l)
#iterate through IPs to add to list, converted to long for sorting
for ip in r:
    iplist.append(iptools.ipv4.ip2long(ip))
iplist = sorted(set(iplist))
iplist = [iptools.ipv4.long2ip(i) for i in iplist]
print "\n".join(iplist)
